package simplest;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

/**
 * This class enables MIPS to be written in a more readable format:
 * 		add_word_to_trie:
 * 		{
 * 			add $t0, $a1, $a2		// This comment will not be shown in final MIPS code.
 * 			lb	$t0, 0($t0)			# This comment will be shown in final MIPS code.
 * 			
 * 			bne	$t0, $0, %AFTER%
 * 			{
 * 				sw $a1, 0($a0)
 * 			}
 * 			...
 * 		}
 * 
 * Grammar: 
 * 	1. Everything other than '{', '}', and "%xxx%" is directly copied into final MIPS code.
 *  2. When using '{...}', if right after "%xxx%" then it should be parsed as a parameter of "%xxx" grammar
 * 	2-1. '%xxx%{...}' is called a "CodeBlock", where "xxx" is called the "tag".
 * 	2-2. "tag" must match "[a-zA-Z0-9_]"
 * 	2-3. "tag" is case-INsensitive
 *  3. "%%" have no function associated with it and will be translated into a '%' character.
 *  4. Available "%xxx%" tags: "%AFTER%"
 *  5. If invalid "%xxx%" exists, stop parsing. 
 *  
 * Output Format:
 * 	Everything other than labels is indented ONLY ONCE.
 * 	labels should NOT be indented.
 * 
 * @author zyu19
 *
 */
public class FormatConverter {
	
	public static void main(String[] args) throws IOException, InterruptedException
	{
		if(args.length!=2)
		{
			System.out.println("Usage:\njava -jar [this jar] [input file] [output file]");
			return;
		}
		Scanner scan = new Scanner(new FileInputStream(args[0]));
		String ans = new FormatConverter().parse(scan.useDelimiter("\\Z").next()).toString();
		scan.close();
		BufferedWriter output = null;
		try {
			output = new BufferedWriter(new FileWriter(args[1]));
			output.write(ans);
		} catch (IOException err) {
			err.printStackTrace();
		} finally {
			if(output!=null) output.close();
		}
	}
	
	public StringBuilder formatNewlines(String src)
	{
		if(!src.contains("\r\n")&&!src.contains("\r")&&!src.contains("\n"))
			return new StringBuilder(src);
		StringBuilder ans = new StringBuilder();
		String[] lines = src.split("(\\r\\n)|(\\n)|(\\r)");
		for(String line : lines)
		{
			if(!line.contains(":"))
				ans.append("\t");
			ans.append(line);
			ans.append("\n");
		}
		return ans;
	}
	
	long nextBlockID = 0;
	
	private int getLineNumber(char[] arr, int k)
	{
		int ans = 1;
		for(int i=0; i<k; i++) {
			if(arr[i]=='\n') ans++;
			else if(arr[i]=='\r') {
				ans++;
				if(i+1<k && arr[i+1]=='\n')
					i++;
			}
		}
		return ans;
	}
	
	private boolean illegalTagChar(char c)
	{
		return (c<'a'||c>'z')&&(c<'A'||c>'Z')&&c!='_'&&c!='.';
	}
	
	public String parse(String mpp) {
		//				Before Parse():
		mpp = mpp.replaceAll("%NEWLINE%", "\n");
		//					Parse():
		StringBuilder ans = new StringBuilder();
		Stack<CodeBlock> helper = new Stack<CodeBlock>();
		
		String lastTag = null;
		StringBuilder curr = ans;
		char[] arr = mpp.toCharArray();
		
		int lastTagBound = -1;		//last index of '%'
		boolean lineJustStarted = true;
		
		for(int i=0; i<arr.length; i++) {
			if(lineJustStarted) {
				boolean hasLabel = false;
				boolean appendTab = false;
				for(int j=i; j<arr.length; j++) {
					if(arr[j]=='\n'||arr[j]=='\r'||arr[j]=='#'||(j+1<arr.length&&arr[j]=='/'&&arr[j+1]=='/')) break;
					if(arr[j]==':') hasLabel = true;
				}
				appendTab = !hasLabel&&(arr[i]==' '||arr[i]=='\t');
				while((arr[i]==' '||arr[i]=='\t')&&i<arr.length)
					i++;
				if(i<arr.length && arr[i]=='.') appendTab = false;
				if(appendTab) curr.append('\t');
				i--;
				lineJustStarted = false;
				continue;
			}
			if(lastTagBound != -1)		//In a Tag !
			{
				if(arr[i]=='%') ;
				else if(illegalTagChar(arr[i]))
				{
					String error = "Illegal Tag Char: " + new String(arr, lastTagBound, i-lastTagBound+1);
					int space = error.length()-1;
					char[] spaces = new char[space];
					Arrays.fill(spaces, ' ');
					error="\n" + error + "\n" + new String(spaces) + "^\n";
					throw new RuntimeException(error);
				}
				else continue;
			}
			if(arr[i]=='%') {
				if(lastTagBound==-1)
					lastTagBound=i;
				else {
					if(lastTagBound+1==i)
						curr.append(arr[i]);
					else {
						lastTag = new String(arr, lastTagBound+1, i-lastTagBound-1);
						int j;
						for(j=i+1; j<arr.length; j++)
							if(arr[j]!=' '&&arr[j]!='\n'&&arr[j]!='\r'&&arr[j]!='\t') break;
						if(j<arr.length&&arr[j]=='{') {
							helper.push(new CodeBlock(lastTag, curr));
							curr = new StringBuilder();
							lastTag = null;
							i=j;
							lastTagBound=-1;
							continue;
						} else {
							if(lastTag.equalsIgnoreCase("AFTER"))
								throw new RuntimeException("\n[Line "+getLineNumber(arr, i)+"] %AFTER% must be followed by {}");
						}
					}
					lastTagBound=-1;
				}
			}
			else if(arr[i]=='{') {
				helper.push(new CodeBlock(lastTag, curr));
				curr = new StringBuilder();
				lastTag = null;
			}
			else if(arr[i]=='}') {
				CodeBlock info = helper.pop();
				if(info.tag==null) {
					info.outside.append(curr);
					curr = info.outside;
				}
				else if(info.tag.equalsIgnoreCase("AFTER")) {
					curr.append("\ncAfter_");	info.outside.append("cAfter_");
					curr.append(nextBlockID);	info.outside.append(nextBlockID);
					curr.append(":\n");
					info.outside.append(curr);
					curr = info.outside;
					nextBlockID++;
				}
				else {
					info.outside.append(curr);
					curr = info.outside;
				}
			}
			else if(arr[i]=='\n') {
				curr.append(arr[i]);
				lineJustStarted = true;
			}
			else if(arr[i]=='\r') {
				curr.append(arr[i]);
				if(i+1<arr.length && arr[i+1]=='\n')
					curr.append(arr[++i]);
				lineJustStarted = true;
			}
			else if(arr[i]=='/') {
				if(i+1<arr.length && arr[i+1]=='/')
					while(i<arr.length&&arr[i]!='\n'&&arr[i]!='\r') i++;
				i--;
				continue;
			}
			else if(arr[i]=='#') {
				while(i<arr.length&&arr[i]!='\n'&&arr[i]!='\r') curr.append(arr[i++]);
				i--;
				continue;
			}
			else {
				curr.append(arr[i]);
			}
		}
		return ans.toString();
	}
}

class CodeBlock {
	String tag;
	StringBuilder outside;
	public CodeBlock(String _tag, StringBuilder builder) {tag = _tag; outside = builder;}
}
