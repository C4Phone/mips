package simplest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

/**
 * This class enables MIPS to be written in a more readable format:
 * 		add_word_to_trie:
 * 		{
 * 			add $t0, $a1, $a2		// This comment will not be shown in final MIPS code.
 * 			lb	$t0, 0($t0)			# This comment will be shown in final MIPS code.
 * 			
 * 			bne	$t0, $0, %AFTER%
 * 			{
 * 				sw $a1, 0($a0)
 * 			}
 * 			...
 * 		}
 * 
 * Grammar: 
 * 	1. Everything other than '{', '}', and "%xxx%" is directly copied into final MIPS code.
 *  2. When using '{...}', if right after "%xxx%" then it should be parsed as a parameter of "%xxx" grammar
 * 	2-1. '%xxx%{...}' is called a "OldCodeBlock", where "xxx" is called the "tag".
 * 	2-2. "tag" must match "[a-zA-Z0-9_]"
 * 	2-3. "tag" is case-INsensitive
 *  3. "%%" have no function associated with it and will be translated into a '%' character.
 *  4. Available "%xxx%" tags: "%AFTER%"
 *  5. If invalid "%xxx%" exists, stop parsing. 
 *  
 * Output Format:
 * 	Everything other than labels is indented ONLY ONCE.
 * 	labels should NOT be indented.
 * 
 * @author zyu19
 *
 */
public class OldFormatConverter {
	
	public static void main(String[] args) throws FileNotFoundException
	{
		System.err.println("abvcdaseg\n".split("(\\r\\n)|(\\n)|(\\r)").length);
		Scanner scan = new Scanner(new FileInputStream("test.s"));
		System.out.println(parse(scan.useDelimiter("\\Z").next()).toString());
	}
	
	public static StringBuilder formatNewlines(String src)
	{
		if(!src.contains("\r\n")&&!src.contains("\r")&&!src.contains("\n"))
			return new StringBuilder(src);
		StringBuilder ans = new StringBuilder();
		String[] lines = src.split("(\\r\\n)|(\\n)|(\\r)");
		for(String line : lines)
		{
			if(!line.contains(":"))
				ans.append("\t");
			ans.append(line);
			ans.append("\n");
		}
		return ans;
	}
	
	static long nextBlockID = 0;
	
	public static String parse(String mpp) {
		StringBuilder ans = new StringBuilder();
		Scanner scan = new Scanner(mpp);
		Stack<OldCodeBlock> helper = new Stack<OldCodeBlock>();
		
		scan.useDelimiter("[\\t ]");
		
		String lastTag = null;
		StringBuilder curr = ans;
		while(scan.hasNext()) {
			String str = scan.next();
			if(str.matches("[\\n\\r]*%[a-zA-Z0-9_]*%[\\n\\r]*"))
			{
				curr.append(formatNewlines(str.substring(0, str.indexOf('%'))));
				
				lastTag = str.substring(str.indexOf('%')+1, str.lastIndexOf('%'));
				
				curr.append(formatNewlines(str.substring(str.lastIndexOf('%')+1, str.length())));
				continue;
			}
			if(str.matches("[\\n\\r]*%[a-zA-Z0-9_]*%[\\n\\r]*\\{[\\n\\r]*"))
			{
				curr.append(formatNewlines(str.substring(0, str.indexOf('%'))));
				
				helper.push(new OldCodeBlock(str.substring(str.indexOf('%')+1, str.lastIndexOf('%')), curr));
				curr = new StringBuilder();
				
				curr.append(formatNewlines(str.substring(str.lastIndexOf('{')+1, str.length())));
				lastTag = null;
				continue;
			}
			
			while(true)
			{
				int l = str.indexOf('{');
				int r = str.indexOf('}');
				if(l==-1 && r==-1)
				{
					curr.append(formatNewlines(str));
					curr.append(' ');
					lastTag = null;
					break;
				}
				if(l!=-1 && r!=-1)
				{
					if(l<r) r=-1;
					else l=-1;
				}
				if(r==-1)		//{
				{
					curr.append(formatNewlines(str.substring(0, l)));
					
					helper.push(new OldCodeBlock(lastTag, curr));
					curr = new StringBuilder();
					lastTag = null;
					
					str = str.substring(l+1);
				}
				if(l==-1)		//}
				{
					curr.append(formatNewlines(str.substring(0, r)));
					
					OldCodeBlock bak = helper.pop();
					if(bak.tag!=null && bak.tag.equalsIgnoreCase("after"))
					{
						String label = "after_" + (nextBlockID++);
						curr.append("\n" + label + ":");
						bak.outside.append(label);
					}
					bak.outside.append(curr);
					curr = bak.outside;
					lastTag = null;
					
					str = str.substring(r+1);
				}
			}
		}
		
		return ans.toString();
	}
}

class OldCodeBlock {
	String tag;
	StringBuilder outside;
	public OldCodeBlock(String _tag, StringBuilder builder) {tag = _tag; outside = builder;}
}
