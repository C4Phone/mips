vert_strncmp:
{
	sub	$sp, $sp, 20
	sw	$s0, 0($sp)
	sw	$s1, 4($sp)
	sw	$s2, 8($sp)
	sw	$s3, 12($sp)
	sw	$ra, 16($sp)

	move $s0, $a0		# $s0 = word + word_iter
	move $s1, $a1		# $s1 = i = start_i
	move $s2, $a2		# $s2 = j

	la	$s3, num_columns
	lw	$s3, 0($s3)		# $s3 = num_columns
	
	vs_loop:
		bge	$s1, $s3, %AFTER%
	{
			move $a0, $s1
			move $a1, $s2
			jal	get_character		# get_character(i,j);

			lb	$t0, 0($s0)			# $t0 = *(word + word_iter) = word[word_iter]
			beq	$v0, $t0, %AFTER%
			{
				move $v0, $0
				j	vs_return
			}
			
			lb	$t0, 1($s0)			# $t0 = *(word + word_iter + 1) = word[word_iter+1]
			bne	$t0, $0, %AFTER%
			{
				mul	$v0, $s1, $s3	# $v0 = i * num_columns
				add	$v0, $v0, $s2	# return ($v0 += j)
				j	vs_return
			}
			
			add	$s1, $s1, 1
			add $s0, $s0, 1
			j	vs_loop
	}
	
	move $v0, $0
vs_return:
	lw	$s0, 0($sp)
	lw	$s1, 4($sp)
	lw	$s2, 8($sp)
	lw	$s3, 12($sp)
	lw	$ra, 16($sp)
	add	$sp, $sp, 20
	jr	$ra
}