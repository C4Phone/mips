














.data


VELOCITY            = 0xffff0010
ANGLE               = 0xffff0014
ANGLE_CONTROL       = 0xffff0018


BOT_X               = 0xffff0020
BOT_Y               = 0xffff0024


PLANETS_REQUEST     = 0xffff1014


SCAN_REQUEST        = 0xffff1010
SCAN_SECTOR         = 0xffff101c


FIELD_STRENGTH      = 0xffff1100


SCORES_REQUEST      = 0xffff1018
ENERGY              = 0xffff1104


PRINT_INT           = 0xffff0080


SCAN_MASK           = 0x2000
SCAN_MASK_SRL8		= 0x20
SCAN_ACKNOWLEDGE    = 0xffff1204
ENERGY_MASK         = 0x4000
ENERGY_MASK_SRL8	= 0x40
ENERGY_ACKNOWLEDGE  = 0xffff1208
CLOCK_MASK			= 0x8000
CLOCK_MASK_SRL8		= 0x80
CLOCK_ACKNOWLEDGE	= 0xffff006c
BONK_MASK			= 0x1000
BONK_MASK_SRL8		= 0x10
BONK_ACKNOWLEDGE	= 0xffff0060


SPIMBOT_PUZZLE_REQUEST 		= 0xffff1000 
SPIMBOT_SOLVE_REQUEST 		= 0xffff1004 
SPIMBOT_LEXICON_REQUEST 	= 0xffff1008 


INTERFERENCE_MASK 	= 0x8000 
INTERFERENCE_ACK 	= 0xffff13048 
SPACESHIP_FIELD_CNT  	= 0xffff110c 


.align 2
iMaxDustCount:	.space 4
iDustMaxSector:	.space 4				## It stores a one-dimensional index, [0, 64)
currVelocity:	.space 4
main_hasEvent:	.space 4
event_Step:		.space 4				## <bool> whether a Clock event happened.
scanReady:		.space 4
latest_scan:	.space 256				## <int[64]> scanResult
scanQueue:		.space 4096				## <int> Queue that stores the ID of the scanned sector of pending Scans
scanQueueMSize		= 4096
scanQueueMSize_Cut4	= 4092
scanQueueRSize:	.space 4				## real size. Used for debug
scanQueueFront:	.space 4				## offset of the element to be poped next.
scanQueueEnd:	.space 4				## offset of the position next new element will be saved at.
iMyStarDeltaThetaHelper: .space 4
s_debug:		.asciiz "[DEBUG]"
s_debug2:		.asciiz ","
.text
scanQueuePop:		

	lw	$t0, scanQueueFront($0)
	la	$v0, scanQueue
	add	$v0, $v0, $t0
	lw	$v0, 0($v0)
	blt	$t0,  scanQueueMSize_Cut4, cAfter_0
	sw	$0, scanQueueFront($0)
	jr	$ra
	
cAfter_0:

	add	$t0, $t0, 4
	sw	$t0, scanQueueFront($0)
	lw	$t0, scanQueueRSize($0)
	sub	$t0, $t0, 1
	sw	$t0, scanQueueRSize($0)
	jr	$ra


scanQueuePush:		

	lw	$t1, scanQueueEnd($0)
	blt	$t1,  scanQueueMSize, cAfter_1
	move $t1, $0
	
cAfter_1:

	la	$t0, scanQueue
	add	$t0, $t0, $t1
	sw	$a0, 0($t0)
	add	$t1, $t1, 4
	sw	$t1, scanQueueEnd($0)
	lw	$t0, scanQueueRSize($0)
	add	$t0, $t0, 1
	sw	$t0, scanQueueRSize($0)
	jr	$ra



main:

DEFINE_Local_Variables:
	
	SIZE_OF_LOCAL_VAR	= 60

	bHasGainedDust			= 0
	iBelievedMaxSector		= 4

	iLastActionedScan		= 12		## It stores ID of a scan after which the latest collection of dust started
	iStickCounter			= 16		## It stores a counter to make bot keep being sticked to something for some time

	sPlanetInfo				= 20		## IMPORTANT: this structure takes 32 bytes

	iFieldOnTime			= 52
	iCounter				= 56

	sub $sp, $sp, SIZE_OF_LOCAL_VAR
	

	
	li	$s0, -4
	sw	$s0, scanQueueFront($0)
	sw	$0 , scanQueueEnd($0)
	sw	$0 , scanQueueRSize($0)

	li	$s0, -1
	sw	$s0, scanReady($0)
	sw	$s0, iMyStarDeltaThetaHelper($0)
	sw	$s0, bHasGainedDust($sp)	## bHasGainedDust must be initialized to True. Or else the bot won't scan at all.
	sw	$0, iMaxDustCount($0)
	sw	$0, iDustMaxSector($0)
	sw	$0, iLastActionedScan($sp)
	sw	$0, iStickCounter($sp)
	sw	$0, iCounter($sp)

	FORWARD_FIELD_TIME	= 1000
	BACKWARD_FIELD_TIME	= 300
	li	$s0, FORWARD_FIELD_TIME
	sw	$s0, iFieldOnTime($sp)

enable_interrupt:
	
	li	$s0, 0xb001		
	mtc0 $s0, $12
	

	lw	$s0, 0xffff001c($0)
	add	$s0, $s0, 50
	sw	$s0, 0xffff001c($0)

	li	$s0, 10
	sw	$s0, VELOCITY($0)
	sw	$s0, currVelocity($0)

	li	$s0, 90
	sw	$s0, ANGLE($0)
	li	$s0, 1
	sw	$s0, ANGLE_CONTROL($0)

	li	$s2, 1				
	li	$s3, 1				

	STICK_TO_PLANET_TIME	= 100
	STICK_TO_DUST_TIME		= 200

	MAIN_FIELD				= 1
	NORMAL_VELOCITY			= 10
	CARRY_VELOCITY			= 8
	FIELD_VELOCITY			= 4
	SLOW_DOWN_VELOCITY		= 2

	DISTANCE_TOLERANCE		= 5

startup_while: bge	 $s3,  100, cAfter_3		
	
	lw	$t0, main_hasEvent($0)
	beq	$t0,  $0, cAfter_2
	sw	$0, main_hasEvent($0)
	add	$s3, $s3, 1
	
cAfter_2:

	
	j	startup_while 
cAfter_3:


	li	$s3, 1
	lw	$t0, iDustMaxSector($0)
	sw	$t0, iBelievedMaxSector($sp)

message_loop:
	
	lw	$s6, main_hasEvent($0)
	lw	$t0, event_Step($0)
	and	$t0, $t0, $s6
	beq	$t0,  $0, cAfter_9
	bne	$s2,  $0, cAfter_6
	add	$a1, $sp, sPlanetInfo
	sw	$a1, PLANETS_REQUEST($0)
	add	$a0, $a1, 16
	jal	avoidStar
	add	$t9, $sp, sPlanetInfo
	lw	$t0, 0($t9)
	lw	$t1, 4($t9)
	lw	$t2, BOT_X($0)
	lw	$t3, BOT_Y($0)
	sub	$a0, $t0, $t2
	sub	$a1, $t1, $t3
	jal	euclidean_dist
	
	
	
	
	bge	$v0,  DISTANCE_TOLERANCE, cAfter_5
	li	$t0, SLOW_DOWN_VELOCITY
	sw	$t0, VELOCITY($0)
	sw	$t0, currVelocity($0)

	add	$s3, $s3, 1
	ble	$s3,  STICK_TO_PLANET_TIME, cAfter_4
	li	$s0, NORMAL_VELOCITY
	sw	$s0, VELOCITY($0)
	sw	$s0, currVelocity($0)
	li	$s2, 1
	li	$s3, 1
	sw	$0, FIELD_STRENGTH($0)
	lw	$t0, iDustMaxSector($0)
	sw	$t0, iBelievedMaxSector($sp)
	
cAfter_4:

	
cAfter_5:

	sw	$0, main_hasEvent($0)
	j message_loop
	
cAfter_6:

	
	lw	$t0, iBelievedMaxSector($sp)
	li	$t1, 8
	div	$t0, $t1
	mfhi $t0
	mflo $t1							
	mul	$t0, $t0, 37
	add	$a0, $t0, 10
	mul	$t1, $t1, 37
	add	$a1, $t1, 10
	
	lw	$t0, BOT_X($0)	
	sub	$a0, $a0, $t0					## $a0 = delta x
	
	lw	$t0, BOT_Y($0)
	sub	$a1, $a1, $t0					## $a1 = delta y
	
	move $s5, $a0
	move $s6, $a1
	
	jal	arctan
	move $a0, $v0
	jal turnAbsolute

	move $a0, $s5
	move $a1, $s5
	jal	euclidean_dist

	bge	$v0,  DISTANCE_TOLERANCE, cAfter_8
	li	$t0, MAIN_FIELD
	sw	$t0, FIELD_STRENGTH($0)
	li	$s0, FIELD_VELOCITY
	sw	$s0, VELOCITY($0)
	sw	$s0, currVelocity($0)

	add	$s3, $s3, 1
	ble	$s3,  STICK_TO_DUST_TIME, cAfter_7
	li	$s0, CARRY_VELOCITY
	sw	$s0, VELOCITY($0)
	sw	$s0, currVelocity($0)
	li	$s2, 0
	li	$s3, 1
	sw	$0, iMaxDustCount($0)
	
cAfter_7:

	
cAfter_8:

	sw	$0, main_hasEvent($0)
	j	message_loop
	
cAfter_9:

	j	message_loop
	
	add $sp, $sp, SIZE_OF_LOCAL_VAR


getMyStarAngVelocity:	

gMSAV_Local_Var:
	
	
	
	gMSAV_TOT_SIZE = 24
	sub	$sp, $sp, gMSAV_TOT_SIZE
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s2, 12($sp)
	sw	$s3, 16($sp)
	sw	$s6, 20($sp)
	

	lw	$s0, 0($a0)
	lw	$s1, 4($a0)

	sub	$a0, $s0, 150
	sub	$a1, $s1, 150
	jal	arctan

	lw	$s3, iMyStarDeltaThetaHelper($0)		## do not modify $s3 after this!
	bge	$s3,  $0, cAfter_10
	sw	$v0, iMyStarDeltaThetaHelper($0)
	move $v0, $0
	j	gMSAV_return
	
cAfter_10:

	
	
	sub	$s6, $v0, 1
	sw	$s6, iMyStarDeltaThetaHelper($0)

	sub	$v0, $v0, $s3
	ble	$v0,  -30, cAfter_12
	bge	$v0,  30, cAfter_11
	sw	$s3, iMyStarDeltaThetaHelper($0)	## undefine $s3
	
cAfter_11:

	
cAfter_12:

	
gMSAV_return:
	
	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s6, 20($sp)
	add	$sp, $sp, gMSAV_TOT_SIZE
	jr	$ra
	


isMyStarCloser:			

iMSC_Local_Var:
	
	
	
	iMSC_TOT_SIZE = 32
	sub	$sp, $sp, iMSC_TOT_SIZE
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s2, 12($sp)
	sw	$s3, 16($sp)
	sw	$s4, 20($sp)
	sw	$s5, 24($sp)
	sw	$s6, 28($sp)
	

	lw	$s0, 0($a0)				## x
	lw	$s1, 4($a0)				## y
	sub	$s4, $s0, 150
	sub	$s5, $s1, 150

Add90DegreeToMyStarCoord3:
	
	
	move $s6, $a0
	jal	getMyStarAngVelocity
	move $a0, $s6
	
	move $s6, $s4
	move $s4, $s5
	move $s5, $s6
	neg	$s4, $s4		## (-y, x)
	
	bge	$v0,  $0, cAfter_13
	neg	$s4, $s4
	neg	$s5, $s5
	
cAfter_13:

		## Stores $s4 = target X, $s5, target Y

	add	$s4, $s4, 150
	add	$s5, $s5, 150

	lw	$s2, BOT_X($0)
	lw	$s3, BOT_Y($0)

	sub	$s4, $s4, $s2
	sub	$s5, $s5, $s3
	abs	$s4, $s4
	abs	$s5, $s5

	add	$s3, $s4, $s5			## $s3 saves Distance(MyPlane+90)

	lw	$s0, 0($a0)				## x
	lw	$s1, 4($a0)				## y
	sub	$s4, $s0, 150
	sub	$s5, $s1, 150

Sub90DegreeToMyStarCoord3:
	
	
	move $s6, $a0
	jal	getMyStarAngVelocity
	move $a0, $s6
	
	move $s6, $s4
	move $s4, $s5
	move $s5, $s6
	neg	$s5, $s5		## (y, -x)
	
	bge	$v0,  $0, cAfter_14
	neg	$s4, $s4
	neg	$s5, $s5
	
cAfter_14:

		## Stores $s4 = target X, $s5, target Y

	move $s6, $s3			## $s6 saves Distance(MyPlane+90)

	add	$s4, $s4, 150
	add	$s5, $s5, 150

	lw	$s2, BOT_X($0)
	lw	$s3, BOT_Y($0)

	sub	$s4, $s4, $s2
	sub	$s5, $s5, $s3
	abs	$s4, $s4
	abs	$s5, $s5

	add	$s5, $s5, $s4

	move $v0, $0
	bge	$s6,  $s5, cAfter_15
	li	$v0, 1
	
cAfter_15:


	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s4, 20($sp)
	lw	$s5, 24($sp)
	lw	$s6, 28($sp)
	add	$sp, $sp, iMSC_TOT_SIZE
	jr	$ra


turnAbsolute:			

	lw	$t0, currVelocity($0)
	bge	$t0,  0, cAfter_16
	add	$a0, $a0, 180
	li	$t0, 360
	div	$a0, $t0
	mfhi $a0
	
cAfter_16:

	sw	$a0, 0xffff0014($0)
	li	$a0, 1
	sw	$a0, 0xffff0018($0)
	jr	$ra



AVOID_SAFE_DISTANCE = 15

avoidStar:				

	aS_STACK_SIZE = 20
	sub	$sp, $sp, aS_STACK_SIZE
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s6, 12($sp)
	sw	$s7, 16($sp)

	move $s6, $a0		
	move $s7, $a1		

	lw	$s0, BOT_X($0)	
	lw	$s1, BOT_Y($0)	

	lw	$t0, 0($a0)		
	lw	$t1, 4($a0)		

	lw	$t2, 0($a1)		
	lw	$t3, 4($a1)		

	sub	$a0, $t0, $s0	
	sub	$a1, $t1, $s1	

	sub	$t6, $t2, $s0	
	sub	$t7, $t3, $s1	

	mul $t8, $a0, $t6
	mul $t9, $a1, $t7
	add	$t9, $t8, $t9	

	ble	$t9,  $0, cAfter_19
	
	
	jal	euclidean_dist

	
	lw	$t9, 12($s6)
	add	$t9, $t9, AVOID_SAFE_DISTANCE

	lw	$t4, 0($s7)		
	lw	$t5, 4($s7)		

	bgt	$v0,  $t9, cAfter_18
	lw	$t0, 0($s6)		
	lw	$t1, 4($s6)		
	
	sub	$a0, $t1, $s1	
	sub	$a1, $s0, $t0	

	sub	$t6, $t4, $s0	
	sub	$t7, $t5, $s1	

	mul	$t8, $a0, $t6
	mul	$t9, $a1, $t7
	add	$t9, $t8, $t9	
	bge	$t9,  $0, cAfter_17
	neg	$a0, $a0
	neg	$a1, $a1
	
cAfter_17:

	jal	arctan
	move $a0, $v0
	jal	turnAbsolute
	j	aS_skipelse_safe
	
cAfter_18:

	
cAfter_19:

	sub	$a0, $t4, $s0
	sub	$a1, $t5, $s1
	jal	arctan
	move $a0, $v0
	jal	turnAbsolute

aS_skipelse_safe:
	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s6, 12($sp)
	lw	$s7, 16($sp)
	add	$sp, $sp, aS_STACK_SIZE
	jr	$ra

arctan:			

	sub	$sp, $sp, 60	## 28 bytes Register + 32 bytes local variable
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s2, 12($sp)
	s.s	$f0, 16($sp)
	s.s	$f1, 20($sp)
	s.s	$f2, 24($sp)

	move $s1, $a0
	move $s2, $a1

	bne	$s1,  $0, cAfter_21
	li	$v0, 90
	bge	$s2,  $0, cAfter_20
	li	$v0, -90
	
cAfter_20:

	j	arctan_return
	
cAfter_21:


	mtc1 $s1, $f0		## $f0 = delta x
	mtc1 $s2, $f1		## $f1 = delta y
	
adjust_range:
	
	li	$a0, 0			## Later we will add the angle from 0
	li	$v0, 1			## We will ADD rather than SUBTRACT
	
	sub	$s0, $s2, $s1	## y-x
	mul	$s0, $s0, $s1	## (y-x)*x
	ble	$s0,  $0, cAfter_22
	mov.s $f2, $f1
	mov.s $f1, $f0
	mov.s $f0, $f2
	li	$a0, 90		## Later we will subtract the angle from 90
	li	$v0, 0		## We will SUBTRACT
	j	avoid_second_if
	
cAfter_22:


	add $s0, $s1, $s2	## x+y
	mul	$s0, $s0, $s1	## (x+y)*x
	bge	$s0,  $0, cAfter_23
	neg.s $f0, $f0
	neg.s $f1, $f1
	mov.s $f2, $f1
	mov.s $f1, $f0
	mov.s $f0, $f2
	li $a0, -90		## Later we will subtract the angle from -90
	li	$v0, 0		## We will SUBTRACT
	
cAfter_23:

	
avoid_second_if:
	bge	$s1,  $0, cAfter_24
	add	$a0, $a0, 180
	
cAfter_24:

	

	cvt.s.w $f0, $f0		## make float
	cvt.s.w $f1, $f1		## make float
	div.s $f0, $f1, $f0		## $f0 = theta = y/x 

	mul.s $f2, $f0, $f0
	mul.s $f2, $f2, $f0		## $f2 = theta**3
	li.s $f1, 3.0
	div.s $f2, $f2, $f1		## $f2 = theta**3/3

	sub.s $f0, $f0, $f2		## $f3 = Taylor_2(arctan(theta))

	
arctan_rotate:
	
	li.s $f1, 57.32		## 57.32 == 180/3.14
	mul.s $f0, $f0, $f1
	cvt.w.s $f0, $f0
	mfc1 $s0, $f0
	bne	$v0,  $0, cAfter_25
	sub $v0, $a0, $s0
	j	dS_rot_skip_else
	
cAfter_25:

	add	$v0, $a0, $s0
dS_rot_skip_else:
	

arctan_return:
	
	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	l.s	$f0, 16($sp)
	l.s	$f1, 20($sp)
	l.s	$f2, 24($sp)
	add $sp, $sp, 60
	jr	$ra
	












euclidean_dist:
	mul	$a0, $a0, $a0	# x^2
	mul	$a1, $a1, $a1	# y^2
	add	$v0, $a0, $a1	# x^2 + y^2
	mtc1	$v0, $f0
	cvt.s.w	$f0, $f0	# float(x^2 + y^2)
	sqrt.s	$f0, $f0	# sqrt(x^2 + y^2)
	cvt.w.s	$f0, $f0	# int(sqrt(...))
	mfc1	$v0, $f0
	jr	$ra




.kdata
.align	2
scanNextToScan:			.word	0
scanPendingScan:		.word	0
lstackTop:				.word	-1
lstack:					.space	36
hasLexicon:				.word	0
sLexicon:				.space	4096
sFullLexicon:			.space	804			
sReversedStr:			.space	7388
.align	2
hl_save: .space	24
hl_except: .asciiz "Exception Occurred.\n"
hl_unknown: .asciiz ": Unknown Interrupt.\n"

STEP_PERIOD			= 100









.ktext	0x80000180
interrupt_handler:
