














.data

// movement memory-mapped I/O
VELOCITY            = 0xffff0010
ANGLE               = 0xffff0014
ANGLE_CONTROL       = 0xffff0018

// coordinates memory-mapped I/O
BOT_X               = 0xffff0020
BOT_Y               = 0xffff0024

// planet memory-mapped I/O
PLANETS_REQUEST     = 0xffff1014

// scanning memory-mapped I/O
SCAN_REQUEST        = 0xffff1010
SCAN_SECTOR         = 0xffff101c

// gravity memory-mapped I/O
FIELD_STRENGTH      = 0xffff1100

// bot info memory-mapped I/O
SCORES_REQUEST      = 0xffff1018
ENERGY              = 0xffff1104

// debugging memory-mapped I/O
PRINT_INT           = 0xffff0080

// interrupt constants
SCAN_MASK           = 0x2000
SCAN_MASK_SRL8		= 0x20
SCAN_ACKNOWLEDGE    = 0xffff1204
ENERGY_MASK         = 0x4000
ENERGY_MASK_SRL8	= 0x40
ENERGY_ACKNOWLEDGE  = 0xffff1208
CLOCK_MASK			= 0x8000
CLOCK_MASK_SRL8		= 0x80
CLOCK_ACKNOWLEDGE	= 0xffff006c
BONK_MASK			= 0x1000
BONK_MASK_SRL8		= 0x10
BONK_ACKNOWLEDGE	= 0xffff0060

// puzzle interface locations 
SPIMBOT_PUZZLE_REQUEST 		= 0xffff1000 
SPIMBOT_SOLVE_REQUEST 		= 0xffff1004 
SPIMBOT_LEXICON_REQUEST 	= 0xffff1008 

// I/O used in competitive scenario 
INTERFERENCE_MASK 	= 0x8000 
INTERFERENCE_ACK 	= 0xffff13048 
SPACESHIP_FIELD_CNT  	= 0xffff110c 


.align 2
iMaxDustCount:	.space 4
iDustMaxSector:	.space 4				## It stores a one-dimensional index, [0, 64)
currVelocity:	.space 4
main_hasEvent:	.space 4
event_Step:		.space 4				## <bool> whether a Clock event happened.
scanReady:		.space 4
latest_scan:	.space 256				## <int[64]> scanResult
scanQueue:		.space 4096				## <int> Queue that stores the ID of the scanned sector of pending Scans
scanQueueMSize		= 4096
scanQueueMSize_Cut4	= 4092
scanQueueRSize:	.space 4				## real size. Used for debug
scanQueueFront:	.space 4				## offset of the element to be poped next.
scanQueueEnd:	.space 4				## offset of the position next new element will be saved at.
iMyStarDeltaThetaHelper: .space 4
s_debug:		.asciiz "[DEBUG]"
s_debug2:		.asciiz ","
.text
scanQueuePop:		// int scanQueuePop()
{
	lw	$t0, scanQueueFront($0)
	la	$v0, scanQueue
	add	$v0, $v0, $t0
	lw	$v0, 0($v0)
	blt	$t0,  scanQueueMSize_Cut4, %AFTER%
	{
		sw	$0, scanQueueFront($0)
		jr	$ra
	}
	add	$t0, $t0, 4
	sw	$t0, scanQueueFront($0)
	lw	$t0, scanQueueRSize($0)
	sub	$t0, $t0, 1
	sw	$t0, scanQueueRSize($0)
	jr	$ra
}

scanQueuePush:		// void scanQueuePush(int val)
{
	lw	$t1, scanQueueEnd($0)
	blt	$t1,  scanQueueMSize, %AFTER%
	{
		move $t1, $0
	}
	la	$t0, scanQueue
	add	$t0, $t0, $t1
	sw	$a0, 0($t0)
	add	$t1, $t1, 4
	sw	$t1, scanQueueEnd($0)
	lw	$t0, scanQueueRSize($0)
	add	$t0, $t0, 1
	sw	$t0, scanQueueRSize($0)
	jr	$ra
}


main:
{
	DEFINE_Local_Variables:
	{
		SIZE_OF_LOCAL_VAR	= 60

		bHasGainedDust			= 0
		iBelievedMaxSector		= 4

		iLastActionedScan		= 12		## It stores ID of a scan after which the latest collection of dust started
		iStickCounter			= 16		## It stores a counter to make bot keep being sticked to something for some time

		sPlanetInfo				= 20		## IMPORTANT: this structure takes 32 bytes

		iFieldOnTime			= 52
		iCounter				= 56

		sub $sp, $sp, SIZE_OF_LOCAL_VAR
	}

	//initialize scan Queue
	li	$s0, -4
	sw	$s0, scanQueueFront($0)
	sw	$0 , scanQueueEnd($0)
	sw	$0 , scanQueueRSize($0)

	li	$s0, -1
	sw	$s0, scanReady($0)
	sw	$s0, iMyStarDeltaThetaHelper($0)
	sw	$s0, bHasGainedDust($sp)	## bHasGainedDust must be initialized to True. Or else the bot won't scan at all.
	sw	$0, iMaxDustCount($0)
	sw	$0, iDustMaxSector($0)
	sw	$0, iLastActionedScan($sp)
	sw	$0, iStickCounter($sp)
	sw	$0, iCounter($sp)

	FORWARD_FIELD_TIME	= 1000
	BACKWARD_FIELD_TIME	= 300
	li	$s0, FORWARD_FIELD_TIME
	sw	$s0, iFieldOnTime($sp)

	enable_interrupt:
	{
		li	$s0, 0xb001		// 0x8000 | 0x2000 | 0x1000 | 0x1 
		mtc0 $s0, $12
	}

	lw	$s0, 0xffff001c($0)
	add	$s0, $s0, 50
	sw	$s0, 0xffff001c($0)

	li	$s0, 10
	sw	$s0, VELOCITY($0)
	sw	$s0, currVelocity($0)

	li	$s0, 90
	sw	$s0, ANGLE($0)
	li	$s0, 1
	sw	$s0, ANGLE_CONTROL($0)

	li	$s2, 1				// $s2 = 0 then AimAtStar. Otherwise, AimAtDust. MUST aim at star first.
	li	$s3, 1				// $s3 is a counter. Counters must start from 1.

	STICK_TO_PLANET_TIME	= 100
	STICK_TO_DUST_TIME		= 200

	MAIN_FIELD				= 1
	NORMAL_VELOCITY			= 10
	CARRY_VELOCITY			= 8
	FIELD_VELOCITY			= 4
	SLOW_DOWN_VELOCITY		= 2

	DISTANCE_TOLERANCE		= 5

	startup_while: bge	 $s3,  100, %AFTER%{		// Startup waiting time.
	{
		lw	$t0, main_hasEvent($0)
		beq	$t0,  $0, %AFTER%
		{
			sw	$0, main_hasEvent($0)
			add	$s3, $s3, 1
		}
	}
	j	startup_while }

	li	$s3, 1
	lw	$t0, iDustMaxSector($0)
	sw	$t0, iBelievedMaxSector($sp)

	message_loop:
	{
		lw	$s6, main_hasEvent($0)
		lw	$t0, event_Step($0)
		and	$t0, $t0, $s6
		beq	$t0,  $0, %AFTER%
		{
			bne	$s2,  $0, %AFTER%
			{
				add	$a1, $sp, sPlanetInfo
				sw	$a1, PLANETS_REQUEST($0)
				add	$a0, $a1, 16
				jal	avoidStar
				add	$t9, $sp, sPlanetInfo
				lw	$t0, 0($t9)
				lw	$t1, 4($t9)
				lw	$t2, BOT_X($0)
				lw	$t3, BOT_Y($0)
				sub	$a0, $t0, $t2
				sub	$a1, $t1, $t3
				jal	euclidean_dist
				//add	$t9, $sp, sPlanetInfo
				//lw	$t9, 12($t9)
				//sub	$t9, $t9, DISTANCE_TOLERANCE
				//bge	$v0,  $t9, %AFTER%
				bge	$v0,  DISTANCE_TOLERANCE, %AFTER%
				{
					li	$t0, SLOW_DOWN_VELOCITY
					sw	$t0, VELOCITY($0)
					sw	$t0, currVelocity($0)

					add	$s3, $s3, 1
					ble	$s3,  STICK_TO_PLANET_TIME, %AFTER%
					{
						li	$s0, NORMAL_VELOCITY
						sw	$s0, VELOCITY($0)
						sw	$s0, currVelocity($0)
						li	$s2, 1
						li	$s3, 1
						sw	$0, FIELD_STRENGTH($0)
						lw	$t0, iDustMaxSector($0)
						sw	$t0, iBelievedMaxSector($sp)
					}
				}
				sw	$0, main_hasEvent($0)
				j message_loop
			}
			// Here $s2 must != 0
			lw	$t0, iBelievedMaxSector($sp)
			li	$t1, 8
			div	$t0, $t1
			mfhi $t0
			mflo $t1							// sector = (x=$t0, y=$t1)
			mul	$t0, $t0, 37
			add	$a0, $t0, 10
			mul	$t1, $t1, 37
			add	$a1, $t1, 10
			
			lw	$t0, BOT_X($0)	
			sub	$a0, $a0, $t0					## $a0 = delta x
			
			lw	$t0, BOT_Y($0)
			sub	$a1, $a1, $t0					## $a1 = delta y
			
			move $s5, $a0
			move $s6, $a1
			
			jal	arctan
			move $a0, $v0
			jal turnAbsolute

			move $a0, $s5
			move $a1, $s5
			jal	euclidean_dist

			bge	$v0,  DISTANCE_TOLERANCE, %AFTER%
			{
				li	$t0, MAIN_FIELD
				sw	$t0, FIELD_STRENGTH($0)
				li	$s0, FIELD_VELOCITY
				sw	$s0, VELOCITY($0)
				sw	$s0, currVelocity($0)

				add	$s3, $s3, 1
				ble	$s3,  STICK_TO_DUST_TIME, %AFTER%
				{
					li	$s0, CARRY_VELOCITY
					sw	$s0, VELOCITY($0)
					sw	$s0, currVelocity($0)
					li	$s2, 0
					li	$s3, 1
					sw	$0, iMaxDustCount($0)
				}
			}
			sw	$0, main_hasEvent($0)
			j	message_loop
		}
		j	message_loop
	}
	add $sp, $sp, SIZE_OF_LOCAL_VAR
}

getMyStarAngVelocity:	//int getMyStarAngVelocity(struct * planet_info); clockwise is POSITIVE.
{
	gMSAV_Local_Var:
	{
		// VAR_SIZE = 0
		// REG_SIZE = 16
		gMSAV_TOT_SIZE = 24
		sub	$sp, $sp, gMSAV_TOT_SIZE
		sw	$ra, 0($sp)
		sw	$s0, 4($sp)
		sw	$s1, 8($sp)
		sw	$s2, 12($sp)
		sw	$s3, 16($sp)
		sw	$s6, 20($sp)
	}

	lw	$s0, 0($a0)
	lw	$s1, 4($a0)

	sub	$a0, $s0, 150
	sub	$a1, $s1, 150
	jal	arctan

	lw	$s3, iMyStarDeltaThetaHelper($0)		## do not modify $s3 after this!
	bge	$s3,  $0, %AFTER%
	{
		sw	$v0, iMyStarDeltaThetaHelper($0)
		move $v0, $0
		j	gMSAV_return
	}
	//add	$s6, $s3, $v0
	//div	$s6, $s6, 2
	sub	$s6, $v0, 1
	sw	$s6, iMyStarDeltaThetaHelper($0)

	sub	$v0, $v0, $s3
	ble	$v0,  -30, %AFTER%
	{
		bge	$v0,  30, %AFTER% {
			sw	$s3, iMyStarDeltaThetaHelper($0)	## undefine $s3
		}
	}
	
	gMSAV_return:
	{
		lw	$ra, 0($sp)
		lw	$s0, 4($sp)
		lw	$s1, 8($sp)
		lw	$s2, 12($sp)
		lw	$s3, 16($sp)
		lw	$s6, 20($sp)
		add	$sp, $sp, gMSAV_TOT_SIZE
		jr	$ra
	}
}

isMyStarCloser:			//bool isMyStarCloser(struct* planet_info);
{
	iMSC_Local_Var:
	{
		// VAR_SIZE = 0
		// REG_SIZE = 16
		iMSC_TOT_SIZE = 32
		sub	$sp, $sp, iMSC_TOT_SIZE
		sw	$ra, 0($sp)
		sw	$s0, 4($sp)
		sw	$s1, 8($sp)
		sw	$s2, 12($sp)
		sw	$s3, 16($sp)
		sw	$s4, 20($sp)
		sw	$s5, 24($sp)
		sw	$s6, 28($sp)
	}

	lw	$s0, 0($a0)				## x
	lw	$s1, 4($a0)				## y
	sub	$s4, $s0, 150
	sub	$s5, $s1, 150

	Add90DegreeToMyStarCoord3:
	{
		// $s6 is a tmp var
		move $s6, $a0
		jal	getMyStarAngVelocity
		move $a0, $s6
		
		move $s6, $s4
		move $s4, $s5
		move $s5, $s6
		neg	$s4, $s4		## (-y, x)
		
		bge	$v0,  $0, %AFTER%
		{
			neg	$s4, $s4
			neg	$s5, $s5
		}
	}	## Stores $s4 = target X, $s5, target Y

	add	$s4, $s4, 150
	add	$s5, $s5, 150

	lw	$s2, BOT_X($0)
	lw	$s3, BOT_Y($0)

	sub	$s4, $s4, $s2
	sub	$s5, $s5, $s3
	abs	$s4, $s4
	abs	$s5, $s5

	add	$s3, $s4, $s5			## $s3 saves Distance(MyPlane+90)

	lw	$s0, 0($a0)				## x
	lw	$s1, 4($a0)				## y
	sub	$s4, $s0, 150
	sub	$s5, $s1, 150

	Sub90DegreeToMyStarCoord3:
	{
		// $s6 is a tmp var
		move $s6, $a0
		jal	getMyStarAngVelocity
		move $a0, $s6
		
		move $s6, $s4
		move $s4, $s5
		move $s5, $s6
		neg	$s5, $s5		## (y, -x)
		
		bge	$v0,  $0, %AFTER%
		{
			neg	$s4, $s4
			neg	$s5, $s5
		}
	}	## Stores $s4 = target X, $s5, target Y

	move $s6, $s3			## $s6 saves Distance(MyPlane+90)

	add	$s4, $s4, 150
	add	$s5, $s5, 150

	lw	$s2, BOT_X($0)
	lw	$s3, BOT_Y($0)

	sub	$s4, $s4, $s2
	sub	$s5, $s5, $s3
	abs	$s4, $s4
	abs	$s5, $s5

	add	$s5, $s5, $s4

	move $v0, $0
	bge	$s6,  $s5, %AFTER%
	{
		li	$v0, 1
	}

	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s4, 20($sp)
	lw	$s5, 24($sp)
	lw	$s6, 28($sp)
	add	$sp, $sp, iMSC_TOT_SIZE
	jr	$ra
}

turnAbsolute:			// void turnAbsolute(int degrees);
{
	lw	$t0, currVelocity($0)
	bge	$t0,  0, %AFTER%
	{
		add	$a0, $a0, 180
		li	$t0, 360
		div	$a0, $t0
		mfhi $a0
	}
	sw	$a0, 0xffff0014($0)
	li	$a0, 1
	sw	$a0, 0xffff0018($0)
	jr	$ra
}


AVOID_SAFE_DISTANCE = 15

avoidStar:				// void avoidStar (struct* avoidedPlanet, struct* anotherPlanet); // If no need to avoid star, it will target at "another" Planet.
{
	aS_STACK_SIZE = 20
	sub	$sp, $sp, aS_STACK_SIZE
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s6, 12($sp)
	sw	$s7, 16($sp)

	move $s6, $a0		// $s6 = avoided
	move $s7, $a1		// $s7 = another

	lw	$s0, BOT_X($0)	// $s0 = botX
	lw	$s1, BOT_Y($0)	// $s1 = botY

	lw	$t0, 0($a0)		// $t0 = avoidX
	lw	$t1, 4($a0)		// $t1 = avoidY

	lw	$t2, 0($a1)		// $t2 = anotherX
	lw	$t3, 4($a1)		// $t3 = anotherY

	sub	$a0, $t0, $s0	// $a0 = vec:a.X
	sub	$a1, $t1, $s1	// $a1 = vec:a.Y

	sub	$t6, $t2, $s0	// $t6 = vec:r.X
	sub	$t7, $t3, $s1	// $t7 = vec:r.Y

	mul $t8, $a0, $t6
	mul $t9, $a1, $t7
	add	$t9, $t8, $t9	// $t9 = a*r

	ble	$t9,  $0, %AFTER%
	{
		// Free: $t8 - $t9
		// Free: $t4 - $t5
		jal	euclidean_dist

		// Free: ALL $tX
		lw	$t9, 12($s6)
		add	$t9, $t9, AVOID_SAFE_DISTANCE

		lw	$t4, 0($s7)		// $t4 = anotherX
		lw	$t5, 4($s7)		// $t5 = anotherY

		bgt	$v0,  $t9, %AFTER%
		{
			lw	$t0, 0($s6)		// $t0 = avoidX
			lw	$t1, 4($s6)		// $t1 = avoidY
			
			sub	$a0, $t1, $s1	// vec:tan.X
			sub	$a1, $s0, $t0	// vec:tan.Y

			sub	$t6, $t4, $s0	// vec:target.X
			sub	$t7, $t5, $s1	// vec:target.Y

			mul	$t8, $a0, $t6
			mul	$t9, $a1, $t7
			add	$t9, $t8, $t9	// $t9 = tan*target
			bge	$t9,  $0, %AFTER%
			{
				neg	$a0, $a0
				neg	$a1, $a1
			}
			jal	arctan
			move $a0, $v0
			jal	turnAbsolute
			j	aS_skipelse_safe
		}
	}
	sub	$a0, $t4, $s0
	sub	$a1, $t5, $s1
	jal	arctan
	move $a0, $v0
	jal	turnAbsolute

	aS_skipelse_safe:
	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s6, 12($sp)
	lw	$s7, 16($sp)
	add	$sp, $sp, aS_STACK_SIZE
	jr	$ra
}
arctan:			// void arctan(int X, int Y);
{
	sub	$sp, $sp, 60	## 28 bytes Register + 32 bytes local variable
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s2, 12($sp)
	s.s	$f0, 16($sp)
	s.s	$f1, 20($sp)
	s.s	$f2, 24($sp)

	move $s1, $a0
	move $s2, $a1

	bne	$s1,  $0, %AFTER%
	{
		li	$v0, 90
		bge	$s2,  $0, %AFTER%
		{
			li	$v0, -90
		}
		j	arctan_return
	}

	mtc1 $s1, $f0		## $f0 = delta x
	mtc1 $s2, $f1		## $f1 = delta y
	
	adjust_range:
	{
		li	$a0, 0			## Later we will add the angle from 0
		li	$v0, 1			## We will ADD rather than SUBTRACT
		
		sub	$s0, $s2, $s1	## y-x
		mul	$s0, $s0, $s1	## (y-x)*x
		ble	$s0,  $0, %AFTER%
		{
			mov.s $f2, $f1
			mov.s $f1, $f0
			mov.s $f0, $f2
			li	$a0, 90		## Later we will subtract the angle from 90
			li	$v0, 0		## We will SUBTRACT
			j	avoid_second_if
		}

		add $s0, $s1, $s2	## x+y
		mul	$s0, $s0, $s1	## (x+y)*x
		bge	$s0,  $0, %AFTER%
		{
			neg.s $f0, $f0
			neg.s $f1, $f1
			mov.s $f2, $f1
			mov.s $f1, $f0
			mov.s $f0, $f2
			li $a0, -90		## Later we will subtract the angle from -90
			li	$v0, 0		## We will SUBTRACT
		}
		
		avoid_second_if:
		bge	$s1,  $0, %AFTER%
		{
			add	$a0, $a0, 180
		}
	}

	cvt.s.w $f0, $f0		## make float
	cvt.s.w $f1, $f1		## make float
	div.s $f0, $f1, $f0		## $f0 = theta = y/x 

	mul.s $f2, $f0, $f0
	mul.s $f2, $f2, $f0		## $f2 = theta**3
	li.s $f1, 3.0
	div.s $f2, $f2, $f1		## $f2 = theta**3/3

	sub.s $f0, $f0, $f2		## $f3 = Taylor_2(arctan(theta))

	
	arctan_rotate:
	{
		li.s $f1, 57.32		## 57.32 == 180/3.14
		mul.s $f0, $f0, $f1
		cvt.w.s $f0, $f0
		mfc1 $s0, $f0
		bne	$v0,  $0, %AFTER%
		{
			sub $v0, $a0, $s0
			j	dS_rot_skip_else
		}
		add	$v0, $a0, $s0
		dS_rot_skip_else:
	}

	arctan_return:
	{
		lw	$ra, 0($sp)
		lw	$s0, 4($sp)
		lw	$s1, 8($sp)
		lw	$s2, 12($sp)
		l.s	$f0, 16($sp)
		l.s	$f1, 20($sp)
		l.s	$f2, 24($sp)
		add $sp, $sp, 60
		jr	$ra
	}
}




// -----------------------------------------------------------------------
// euclidean_dist - computes sqrt(x^2 + y^2)
// $a0 - x
// $a1 - y
// returns the distance
// -----------------------------------------------------------------------

euclidean_dist:
	mul	$a0, $a0, $a0	# x^2
	mul	$a1, $a1, $a1	# y^2
	add	$v0, $a0, $a1	# x^2 + y^2
	mtc1	$v0, $f0
	cvt.s.w	$f0, $f0	# float(x^2 + y^2)
	sqrt.s	$f0, $f0	# sqrt(x^2 + y^2)
	cvt.w.s	$f0, $f0	# int(sqrt(...))
	mfc1	$v0, $f0
	jr	$ra



// Kernel must be included in the very end.
.kdata
.align	2
scanNextToScan:			.word	0
scanPendingScan:		.word	0
lstackTop:				.word	-1
lstack:					.space	36
hasLexicon:				.word	0
sLexicon:				.space	4096
sFullLexicon:			.space	804			// sRevStr is part of sFullLex
sReversedStr:			.space	7388
.align	2
hl_save: .space	24
hl_except: .asciiz "Exception Occurred.\n"
hl_unknown: .asciiz ": Unknown Interrupt.\n"

STEP_PERIOD			= 100

// Macros
// For all these macro functions, only $k0 are available.




// End of Macros

.ktext	0x80000180
interrupt_handler:
{
	.set noat
	move $k1, $at
	.set at
	
	la	$k0, hl_save
	sw	$a0, 0($k0)
	sw	$v0, 4($k0)
	sw	$t0, 8($k0)
	sw	$t1, 12($k0)
	sw	$t2, 16($k0)
	sw	$t3, 20($k0)

	handler_core:
	{
		mfc0 $t0, $13		## Load Cause Register
		srl	$t1, $t0, 2 
		and	$t1, $t1, 0x1f	## $t1 = Exception Code 
		srl	$t0, $t0, 8		
		and $t0, $t0, 0xff	## $t0 = Interrupt Code

		or	$a0, $t0, $t1	## $a0 indicates whether everything is done
		bne	$a0,  0, %AFTER%
		{
			j	handler_return
		}
		beq	$t1,  0, %AFTER%
		{
			li	$v0, 4
			la	$a0, hl_except
			syscall
			// After Checking Exception, we should continue.
		}
		and	$t1, $t0, CLOCK_MASK_SRL8	## IMPORTANT: $t0 has been shifted.
		beq	$t1,  0, %AFTER%
		{
			// If it is a clock interrupt
			//tryScan					// void tryScan();
			{
				lw	$k0, scanReady($0)
				bne	$k0,  $0, %AFTER%
				{
					// If last Scan is not finished, wait.
					j	CLOCKskip_tryScan
				}
			
				lw	$k0, scanNextToScan($0)
				blt	$k0,  64, %AFTER%
				{
					move $k0, $0
				}
				add	$v0, $k0, 1
				sw	$k0, scanPendingScan($0)
				sw	$v0, scanNextToScan($0)

				//requestScan
				{
					sw	$k0, SCAN_SECTOR($0)
					la	$v0, latest_scan
					sw	$v0, SCAN_REQUEST($0)
					sw	$0, scanReady($0)
				}
			}
			CLOCKskip_tryScan:

			// solvePuzzles
			{
				la	$v0, hasLexicon
				lw	$k0, 0($v0)
				bne	$k0,  $0, %AFTER%
				{
					sw	$v0, hasLexicon($0)
					la	$v0, sLexicon
					sw	$v0, SPIMBOT_LEXICON_REQUEST($0)
					la	$t3, sReversedStr

					lw	$t0, 0($v0)
					li	$t1, 0
					CLOCK_lexicon_loop: bge	 $t1,  $t0, %AFTER%{
					{
						la	$k0, sFullLexicon

						mul	$a0, $t1, 4
						add	$a0, $a0, $v0
						lw	$a0, 4($a0)
						mul	$t2, $t1, 8
						add	$t2, $t2, $k0
						sw	$a0, 4($t2)
						sw	$t3, 8($t2)
						Cll_copy1:
						{
							lb	$t2, 0($a0)
							beq	$t2, $0, Cll_skip_copy1
							%NEWLINE%{ 												%NEWLINE%	lw	$k0, lstackTop($0)						%NEWLINE%	add	$k0, $k0, 1								%NEWLINE%	sw	$t2, lstack($k0)					%NEWLINE%	sw	$k0, lstackTop($0)						%NEWLINE%}
							add	$a0, $a0, 1
						}
						Cll_skip_copy1:
						
						%NEWLINE%{ 												%NEWLINE%	lw	$a0, lstackTop($0)				%NEWLINE%	add	$a0, $a0, 1				%NEWLINE%}
						Cll_copy2: beq	 $a0,  $0, %AFTER%{
						{
							%NEWLINE%{ 												%NEWLINE%	lw	$k0, lstackTop($0)						%NEWLINE%	lw	$t2, lstack($k0)					%NEWLINE%	sub	$k0, $k0, 1								%NEWLINE%	sw	$k0, lstackTop($0)						%NEWLINE%}
							sb	$t2, 0($t3)
							add	$t3, $t3, 1
							%NEWLINE%{ 												%NEWLINE%	lw	$a0, lstackTop($0)				%NEWLINE%	add	$a0, $a0, 1				%NEWLINE%}
						}
						sb	$0, 0($t3)
						add	$t3, $t3, 1

						add	$t1, $t1, 1
					}
					j	CLOCK_lexicon_loop }
					j	CLOCKskip_solvePuzzles
				}
			}
			CLOCKskip_solvePuzzles:

			sw	$0, CLOCK_ACKNOWLEDGE($0)
			lw	$k0, 0xffff001c($0)
			add	$k0, $k0, STEP_PERIOD
			sw	$k0, 0xffff001c($0)
			la	$k0, event_Step
			sw	$k0, 0($k0)				## Set event_Step to a non-zero value
			j	handler_core
		}
		and	$t1, $t0, SCAN_MASK_SRL8
		beq	$t1,  0, %AFTER%
		{
			// If it is a scan interrupt
			sw	$a0, SCAN_ACKNOWLEDGE($0)
			lw	$t1, scanPendingScan($0)
			// $t1 stores the updated sector's ID
			mul	$k0, $t1, 4
			la	$v0, latest_scan
			add	$k0, $k0, $v0	// $k0 = &latest_scan[ID]
			lw	$k0, 0($k0)				// $k0 = latest_scan[ID]
			lw	$v0, iMaxDustCount($0)
			ble	$k0,  $v0, %AFTER%
			{
				sw	$k0, iMaxDustCount($0)
				sw	$t1, iDustMaxSector($0)
				sw	$t1, PRINT_INT($0)
			}
			//doNextScan
			{
				lw	$k0, scanNextToScan($0)
				blt	$k0,  64, %AFTER%
				{
					move $k0, $0
				}
				add	$v0, $k0, 1
				sw	$k0, scanPendingScan($0)
				sw	$v0, scanNextToScan($0)

				//tS_requestScan
				{
					sw	$k0, SCAN_SECTOR($0)
					la	$v0, latest_scan
					sw	$v0, SCAN_REQUEST($0)
					sw	$0, scanReady($0)
				}
			}
			j	handler_core
		}
		and $t1, $t0, BONK_MASK_SRL8
		beq	$t1,  0, %AFTER%
		{
			// If it is a bonk interrupt
			sw	$a0, BONK_ACKNOWLEDGE($0)

			lw	$k0, currVelocity($0)
			neg	$k0, $k0
			sw	$k0, VELOCITY($0)
			sw	$k0, currVelocity($0)
			sw	$0, FIELD_STRENGTH($0)
			j	handler_core
		}
		// In this case, there is an unknown interrupt that happened.
		li	$v0, 1
		move $a0, $t0
		syscall
		li	$v0, 4
		la	$a0, hl_unknown
		syscall

		j	handler_return
	}
	
	handler_return:
	{
		la	$t0, main_hasEvent
		sw	$t0, 0($t0)				## Set main_hasEvent to a non-zero value
		
		la	$k0, hl_save
		lw	$a0, 0($k0)
		lw	$v0, 4($k0)
		lw	$t0, 8($k0)
		lw	$t1, 12($k0)
		lw	$t2, 16($k0)
		lw	$t3, 20($k0)
	
		.set noat
		move $at, $k1
		.set at
	
		eret
	}
}

