find_words:
	
	sub $sp, $sp, 44
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $a1, 8($sp)
	sw $s0, 12($sp)
	sw $s1, 16($sp)
	sw $s2, 20($sp)
	sw $s3, 24($sp)
	sw $s4, 28($sp)
	sw $s5, 32($sp)
	sw $s6, 36($sp)
	sw $s7, 40($sp)
	move $s0, $0 # $s0 = i <- 0
	la $s1, num_rows
	lw $s1, 0($s1) # $s1 = num_rows
	la $s2, num_columns
	lw $s2, 0($s2) # $s2 = num_columns
fw_loop1: bge $s0, $s1, cAfter_4
	
	move $s3, $0 # $s3 = j <- 0
fw_loop2: bge $s3, $s2, cAfter_3
	
	mul $s4, $s0, $s2
	add $s5, $s4, $s2
	add $s4, $s4, $s3 # $s4 = start
	sub $s5, $s5, 1 # $s5 = end
	move $s6, $0 # $s6 = k <- 0
fw_loop3: bge $s6, $a1, cAfter_2
	
	mul $s7, $s6, 4
	add $s7, $a0, $s7
	lw $s7, 0($s7) # $s7 = word
	move $a0, $s7
	move $a1, $s4
	move $a2, $s5
	jal horiz_strncmp
	ble $v0, $0, cAfter_0
	move $a0, $s7
	move $a1, $s4
	move $a2, $v0
	jal record_word
	
cAfter_0:

	move $a0, $s7
	move $a1, $s0
	move $a2, $s3
	jal vert_strncmp
	ble $v0, $0, cAfter_1
	move $a0, $s7
	move $a1, $s4
	move $a2, $v0
	jal record_word
	
cAfter_1:

	lw $a0, 4($sp)
	lw $a1, 8($sp)
	add $s6, $s6, 1
	
	j fw_loop3 
cAfter_2:

	add $s3, $s3, 1
	
	j fw_loop2 
cAfter_3:

	add $s0, $s0, 1
	
	j fw_loop1 
cAfter_4:

	lw $ra, 0($sp)
	lw $a0, 4($sp)
	lw $a1, 8($sp)
	lw $s0, 12($sp)
	lw $s1, 16($sp)
	lw $s2, 20($sp)
	lw $s3, 24($sp)
	lw $s4, 28($sp)
	lw $s5, 32($sp)
	lw $s6, 36($sp)
	lw $s7, 40($sp)
	add $sp, $sp, 44
	jr $ra
	