if [ $# -ne 2 ];
then
	echo -e "Usage:\n $0 [input file] [output file]"
	exit
fi

gcc -E -x c -P -traditional-cpp -include ~/Bin/_mips++/common.h $1 -o $2
java -jar ~/Bin/fconv.jar $2 $2
